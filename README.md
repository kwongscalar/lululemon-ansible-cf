# Lululemon Ansible Cloudformation #

This is the repo used to orchestrate the running of the three Cloudformation templates which were
created for Lululemon.

### Requirements ###

* Ansible 2.2+
* Boto 2.4.x
* Access to Lululemon or Scalar AWS account

### How does it work ###

Passing variables between separate CloudFormation templates is clunky at best.  Ansible provides
a more elegant solution by not only orchestrating the running of the CF templates into a single
command, but also capturing the outputs of the CF templates into variables which can
be used in other templates.

### To run the playbook ###

Creating a stack
`ansible-playbook lululemon-cf.yml --extra-vars "stack_env=qa"`

Destroying a stack
`ansible-playbook lululemon-cf-destroy.yml --extra-vars "stack_env=qa"`

Remember to have a var file referencing the stack env name in the `vars` directory (see Variables section)

### Variables ###

Consult `roles/cloudformation/defaults/main.yml.default` to create your own copy of the
variables into vars directory. This controls the CloudFormation creation/destroying

### CloudFormation Templates ###

Make modifications under `cf-templates` directory. Ensure the names correspond to your variables file
