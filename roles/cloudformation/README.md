cloudformation
==============

Ansible Role for Lululemon CloudFormation Stack Creation

## Requirements

* Ansible 2.2
* Boto 2.4.x (and either AWS env or AWS Profile configured, can use `--boto-profile=<profile>` if multiple profiles exist for aws cli/boto)
* Rights to gather EC2, VPC and CloudFormation facts, as well as execute CloudFormation

## Role Variables

Copy `roles/main.yml.default`, update it to whatever settings are
required and then include the vars file before executing the role.
Variables can also be in-line but they must be declared.
