cloudformation-destroy
======================

Ansible Role for generic CloudFormation Stack destruction

## Requirements ##

* Ansible 2.2
* Boto 2.4.x (and either AWS env or AWS Profile configured, can use `--boto-profile=<profile>` if multiple profiles exist for aws cli/boto)
* Rights to execute CloudFormation

## Role Variables ##

These variables are assumed when you execute the tasks:

`region` - where the CloudFormation lives
`stacks` - array of CloudFormation stacks to destroy

You may also want to consider ignoring errors.

## Example ##

```    - role: cloudformation-destroy
      region: us-east-1
      stacks:
      - "stack1"
      - "stack2"
```
